package org.ws4d.java;

import org.ws4d.java.util.Log;

import framboos.OutPin;

public class LEDThread extends Thread {	
	
	public final byte off = 1;
	public final byte on = 2;
	public final byte blink1 = 3;
	public final byte blink2 = 4;
	public final byte blink3 = 5;
	public final byte busyblink = 6;
	public final byte lowblink = 7;
	public final byte recorderReady = 8;
	
	private byte state = off;
	
	private OutPin ledPin = null;
	
	
	
	public LEDThread(OutPin ledPin){
		
		this.ledPin = ledPin;
		
	}
	
	
		
	public void run ()
	{
			
		while(true){
			switch(state){
			
			case on:
				while(state==on){
					if(ledPin.getValue()==false){
						ledPin.setValue(true);
					}
					pause(200);
				}
				break;	
			
			
			case off:
				while(state==off){
					if(ledPin.getValue()==true){
						ledPin.setValue(false);
					}
					pause(200);
				}
				break;			
			
			case blink1:
				while(state==blink1){
					short[] b1 = {200,2000};			
					onOffSequence(ledPin, b1);
				}	
				break;
				
			case blink2:
				while(state==blink2){
					short[] b2 = {200,200,200,1500};			
					onOffSequence(ledPin, b2);
				}	
				break;
				
			case blink3:
				while(state==blink3){
					short[] b3 = {200,200,200,200,200,1500};			
					onOffSequence(ledPin, b3);
				}	
				break;
			
			case busyblink:
				while(state==busyblink){
					short[] bb = {100,100};			
					onOffSequence(ledPin, bb);
				}	
				break;
			
			case lowblink:
				while(state==lowblink){
					short[] lb = {500,500};			
					onOffSequence(ledPin, lb);
				}					
				break;
				
			case recorderReady:
				while(state==recorderReady){
					short[] rr = {1400,100};			
					onOffSequence(ledPin, rr);
				}					
				break;	
				
				
			
				
			/** to be implemented **/
			
		}
			
			
			
			
		}			
	}
		
		
		
		
		
	public void switchState(byte nuState){
		if(this.state!=nuState){		
			this.state=nuState;
		}		
	}
	
	public byte getCurrentState(){
		
		return this.state;
		
	}
	
	
	private void pause(int ms){
		
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/** Outputs a blink sequence - odd elements: led on for ms of element , even elements: led off for ms of element**/
	private static void onOffSequence(OutPin led, short[] a){
		
		if(((a.length)%2)!=0){
			Log.error("LEDThread: Array must be even");
			return;			
		}
		
		for(int i=0;i<=a.length-2;i=i+2){
			led.setValue(true);
			try {
				Thread.sleep(a[i]);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			led.setValue(false);
			try {
				Thread.sleep(a[i+1]);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			
			
		}
		return;		
	}

}
