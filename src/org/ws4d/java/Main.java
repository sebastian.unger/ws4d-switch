package org.ws4d.java;

import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.incubation.CredentialManagement.SecurityContext;
import org.ws4d.java.incubation.CredentialManagement.SecurityToken;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.authenticationclient.DefaultAuthenticationClient;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ClientAuthenticationCallbacks;
import org.ws4d.java.incubation.wscompactsecurity.authorization.authorizationclient.DefaultAuthorizationClient;
import org.ws4d.java.incubation.wscompactsecurity.authorization.types.AuthorizationDecision;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import framboos.InPin;
import framboos.OutPin;
import framboos.algorithm.util.Timer;
import framboos.util.HardReset;


/****************************************************
 *  do NOT Remove
 *  correct setup for addressing the input switch:
 *
 *  GPIO: 1
 *  KeyLength: 20
 *  Symbol length: 1000 = 1s;
 *
 *  SwitchKeyRecorder.record_int_key(1, 20, 1000);
 * 
 ****************************************************/

public class Main {

	/********************         GPIO                *************/

	final private static byte switchGpioPort = 1;
	final private static byte ledGpioPort = 0;

	/********************      OOB-Key settings       *************/

	final private static int OOBKeyLength = 20;
	final private static int OOBSymbolLength = 1000; //=1s per symbol


	/**************   Device names + name spaces       *************/
	static {
		AuthenticationEngine.setDefaultOwnerID("urn:uuid:822a81bb-e6bf-4ce2-a50a-09c949d315d2");
	}

	final private static String NAMESPACE = WSSecurityForDevicesConstants.NAMESPACE;
	final private static String LIGHTBULB_DEVICENAME = "LightbulbDevice";
	final private static String UIAUTHENTICATOR_DEVICENAME = WSSecurityForDevicesConstants.UIAuthenticatorType;
	final private static String AUTHENTICATOR_DEVICENAME = WSSecurityForDevicesConstants.AuthenticatorType;

	final public static QName UIAUTHENTICATOR_DEVICE_Q_NAME = new QName(UIAUTHENTICATOR_DEVICENAME, NAMESPACE);
	final public static QName LIGHTBULB_DEVICE_Q_NAME = new QName(LIGHTBULB_DEVICENAME, NAMESPACE);

	final public static QName AUTHENTICATOR_DEVICE_Q_NAME = new QName(AUTHENTICATOR_DEVICENAME, NAMESPACE);


	/**************   Filename for stored key+device   *************/

	final private static String FILENAME = "/home/pi/LastDevice.LightBulb";	// name of the file that stores the key+UUID on drive


	/**************************   States   ************************/


	final private static byte check_existing_key = 1;
	final private static byte authentication = 2;
	final private static byte toggle_bulb = 3;
	final private static byte no_existing_key = 4;
	final private static byte delete_key = 5;
	final private static byte bulbdiscovery = 6;
	final private static byte authorization = 7;

	private static byte state = check_existing_key;


	public static void main(String[] args){

		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		Log.setShowTimestamp(true);

		Log.info("[..H..] Main: Initializing... ");

		if (HardReset.hardResetPin(ledGpioPort))
			Log.info("[..H..] Main: Did hard-reset LED-Pin: "+ ledGpioPort);
		if (HardReset.hardResetPin(switchGpioPort))
			Log.info("[..H..] Main: Did hard-reset Switch-Pin: "+ switchGpioPort);

		final InPin switchPin = new InPin(switchGpioPort);
		final OutPin ledPin = new OutPin(ledGpioPort);

		final LEDThread ledThread = new LEDThread(ledPin);
		ledThread.switchState(ledThread.lowblink);
		ledThread.start();

		ClientAuthenticationCallbacks authenticationcb = new ClientAuthenticationCallbacks() {
			@Override
			public int getOOBpinAsInt(QName mechanism) {
				byte oldLedState = ledThread.getCurrentState();
				ledThread.switchState(ledThread.recorderReady);

				int intOOBss = -1;
				intOOBss = SwitchKeyRecorder.record_int_key(switchPin, OOBKeyLength, OOBSymbolLength);

				ledThread.switchState(oldLedState);
				return intOOBss;
			}

			@Override
			public void deliverAuthenticationData(SecurityContext ct) {
				AuthenticationEngine ae = AuthenticationEngine.getInstance();
				ae.getContextDatabase().addContext(ct);
				ae.saveSecurityAssociations();
			}

			@Override
			public void prereportMechanism(QName mechanism) {
				// TODO Auto-generated method stub

			}

			@Override
			public HashMap receiveAdditionalAuthenticationData() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void provideAdditionalAuthenticationData(Object key,
					Object data) {
				// TODO Auto-generated method stub

			}
		};

		state = check_existing_key;

		JMEDSFramework.start(null);
		// CLient + HelloListener
		DeviceFoundInterface deviceFoundCallback = new DeviceFoundInterface() {
			private Device bulb = null;
			private Device uiauth = null;
			private Device auth = null;
			@Override
			public Device getBulb() {
				return bulb;
			}
			@Override
			public void resetBulb() {
				this.bulb = null;
			}
			@Override
			public void overrideBulb(Device device) {
				bulb = device;
			}
			@Override
			public Device getUIAuth() {
				return uiauth;
			}
			@Override
			public void resetUIAuth() {
				this.uiauth = null;
			}
			@Override
			public void overrideUIAuth(Device device) {
				uiauth=device;
			}
			@Override
			public boolean discoveryComplete() {
				return (bulb != null) && ((uiauth!=null) || (auth!=null));
			}
			@Override
			public boolean discoveryIncomplete() {
				return !discoveryComplete();
			}
			@Override
			public void deviceFound(QName type, Device device) {
				if (uiauth == null && UIAUTHENTICATOR_DEVICE_Q_NAME.equals(type)) {
					Log.info("[..H..] NEW UI-Authenticator discovered");
					uiauth = device;
				}
				if (auth == null && AUTHENTICATOR_DEVICE_Q_NAME.equals(type)) {
					Log.info("[..H..] NEW Authenticator discovered");
					auth = device;
				}
				if (bulb == null && LIGHTBULB_DEVICE_Q_NAME.equals(type)) {
					Log.info("[..H..] NEW LightBulb discovered");
					bulb = device;
				}
			}
			@Override
			public Device getAuth() {
				return auth;
			}
			@Override
			public void resetAuth() {
				auth = null;

			}
			@Override
			public void overrideAuth(Device device) {
				auth = device;
			}
			@Override
			public Device getAnyAuth() {
				// TODO Auto-generated method stub
				return (auth != null) ? auth : uiauth;
			}
		};

		/* configuring supported mechanisms and algorithms */
		AuthenticationEngine ae = AuthenticationEngine.getInstance();
		ae.addSupportedOOBauthenticationMechanism(new QName(WSSecurityForDevicesConstants.TappingAuthentication, WSSecurityForDevicesConstants.NAMESPACE));

		DiscoveryClient discoveryClient = new DiscoveryClient(deviceFoundCallback);

		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_AES_CBC128)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_RC4)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						null
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						null
						)
				);


		state = bulbdiscovery;

		/****************************************************************
		 *              STATEMACHINE - the actual program
		 ***************************************************************/

		while(true){

			switch (state) {

			case bulbdiscovery:
				discoveryClient.start();
				Log.info("[..H..] Main: State: bulbdiscovery");
				ledThread.switchState(ledThread.busyblink);
				while (deviceFoundCallback.getBulb() == null) {
					pause(500);
				}
				state = check_existing_key;
				break;

			case check_existing_key:
				Log.info("[..H..] Main: State: check_existing_key");
				ledThread.switchState(ledThread.busyblink);
				ae.setDbName(FILENAME);
				if(ae.loadSecurityAssociations()){
					SecurityContext sc = ae.getContextDatabase().getContextById(deviceFoundCallback.getBulb().getEndpointReference().getAddress().toString());
					SecurityToken st = (sc == null) ? null : sc.getSecurityToken();
					if (st == null) {
						state=no_existing_key;
					} else {
						state=toggle_bulb;
					}
				}else{
					state=no_existing_key;
				}
				break;

			case authentication:

				Log.info("[..H..] Main: State: authentication");
				Log.info("[..H..] Stopping discovery");
				discoveryClient.stop();
				ledThread.switchState(ledThread.lowblink);
				int success = -1;

				if(deviceFoundCallback.discoveryComplete()){
					DefaultAuthenticationClient client = new DefaultAuthenticationClient();
					client.setAuthenticationCallbacks(authenticationcb);
					//FIXME: Seriously! Do another level, so I don't have to pass Devices' References to get resolved again...
					success = client.authenticate(deviceFoundCallback.getBulb().getDeviceReference(SecurityKey.EMPTY_KEY), null);
				}else{
					Log.error("[..H..] Main: Device-NULL Error, something went terribly wrong at discovery process :/ reseting devices"   );
					deviceFoundCallback.resetUIAuth();
					deviceFoundCallback.resetBulb();
					deviceFoundCallback.resetAuth();
					state=bulbdiscovery;
					break;
				};


				switch (success){
				case DefaultAuthenticationClient.ERR_NO_ERROR:
					Log.info("[..H..] Main: Key Exchange Successfull");
					state = check_existing_key;
					break;
				case DefaultAuthenticationClient.ERR_COMMUNICATION:
					deviceFoundCallback.resetUIAuth();
					deviceFoundCallback.resetAuth();
					state = check_existing_key;
					break;
				default:
					Log.error("[..H..] Main: Key Exchange FAILED");
					deviceFoundCallback.resetUIAuth();
					deviceFoundCallback.resetAuth();
					state = no_existing_key;
				}

				break;


			case toggle_bulb:

				ledThread.switchState(ledThread.off);
				discoveryClient.stop();
				Log.info("[..H..] Main: State: toggle_bulb");

				int result = ToggleBulbClient.startToggleBulbListener(deviceFoundCallback.getBulb(), ledThread, switchPin);

				switch (result) {
				case 10: /* button pressed for 7sec */
					state = delete_key;
					break;
				case ToggleBulbClient.ERR_AUTHENTICATION: /* invocation delivered authentication failure */
					ledThread.switchState(ledThread.busyblink);
					/* apparently a former working authentication has been revoked. Delete context then */
					AuthenticationEngine.getInstance().getContextDatabase().removeContext(deviceFoundCallback.getBulb().getEndpointReference().getAddress().toString());
					AuthenticationEngine.getInstance().saveSecurityAssociations();
					state = no_existing_key;
					break;
				case ToggleBulbClient.ERR_AUTHORIZATION: /* Authentication okay but authorization not sufficient */
					state = authorization;
					break;
				default:
					ledThread.switchState(ledThread.busyblink);
					deviceFoundCallback.resetUIAuth();
					deviceFoundCallback.resetBulb();
					deviceFoundCallback.resetAuth();
					state = bulbdiscovery;
					break;
				}


				break;


			case no_existing_key:

				Log.info("[..H..] Main: State: no_existing_key");

				ledThread.switchState(ledThread.busyblink);
				discoveryClient.start();

				/** prevents Authentication from being started if Button is still pushed after deleting key file **/
				AuthenticationTriggerPreFilter(switchPin);

				/******************************************************
				 * choosing the initial led behavior:
				 * bulb discovered->blink1,
				 * authenticator discovered->blink2,
				 * both discovered ->blink3
				 * none discovered ->led on
				 *******************************************************/

				Log.info("[..H..] Main: Waiting for Devices to become ready");

				while (deviceFoundCallback.discoveryIncomplete()) {
					if((deviceFoundCallback.getBulb()==null)&&(deviceFoundCallback.getAnyAuth()==null)){
						ledThread.switchState(ledThread.on); 		// none discovered
					}else if((deviceFoundCallback.getBulb()!=null)&&(deviceFoundCallback.getAnyAuth()!=null)){
						ledThread.switchState(ledThread.blink3);	// both discovered
					}else if(deviceFoundCallback.getBulb()!=null){
						ledThread.switchState(ledThread.blink1);	// led discovered
					}else{
						ledThread.switchState(ledThread.blink2);	// authenticator discovered
					}
					pause(100);
				}
				ledThread.switchState(ledThread.blink3);	// both discovered

				Log.info("[..H..] Main: Waiting for Trigger ");

				int count = 0;
				while(switchPin.getValue()==false){
					pause(100);
					if ((++count) % 100 == 0)
						Log.info("[..H..] Main: Waiting for Trigger ");
					else if ((++count) % 10 == 0)
						Log.debug("[..H..] Main: Waiting for Trigger ");
				}

				/*******************************************************
				 * push button for 7seconds to delete
				 * discovered devices
				 * 
				 * reason: if the device we want to authenticate with
				 * is restarted at this point, the authentication wont work.
				 * if we don't want to restart the switch, we have to delete the discovered devices
				 *******************************************************/
				//TODO: This (and many other things) can be avoided by proper handling of device state changes
				//      e.g. catch bye-message -> callback to set device null -> take action in loop above
				int abort_count = 0;
				while((switchPin.getValue()==true)&&(abort_count<700)){	// test if button is pushed for 7seconds
					abort_count++;
					pause(10);
				}

				if(abort_count>=700){					//if pushed for 7seconds: abort, else: try to switch bulb
					Log.info("reseting disco client");
					deviceFoundCallback.resetUIAuth();
					deviceFoundCallback.resetAuth();
					deviceFoundCallback.resetBulb();
					state=bulbdiscovery;
					break;
				}

				state=authentication;

				break;



			case delete_key:

				Log.info("[..H..] Main: State: delete_key");
				ledThread.switchState(ledThread.busyblink);

				AuthenticationEngine.getInstance().removeAllSecurityAssociations();
				deviceFoundCallback.resetAuth();
				deviceFoundCallback.resetUIAuth();

				// delete key here
				state = no_existing_key;

				break;

			case authorization:

				Log.info("[..H..] Main: State: Authorization");
				ledThread.switchState(ledThread.lowblink);

				DefaultAuthorizationClient defauthzclient = new DefaultAuthorizationClient();

				AuthorizationDecision decision = AuthorizationDecision.INDETERMINATE;

				try {
					decision = defauthzclient.authorize(deviceFoundCallback.getBulb().getDeviceReference(SecurityKey.EMPTY_KEY));
				} catch (AuthorizationException | CommunicationException
						| InvocationException e) {
					e.printStackTrace();
				}

				if (decision == AuthorizationDecision.INDETERMINATE) {
					/* FIXME!!
					 * ---------------------------------------------
					 * 
					 * Well, there is a problem here...
					 * JMEDS already checked the target STS earlier to look
					 * for its brokers. At that point, there was no Authorizer-
					 * statement in the Policies yet. However, JMEDS won't fetch
					 * the WSDL again. Therefore, use this hack to make it work.
					 * Sorry.
					 * 
					 * It could possibly be solved by implementing a device
					 * listener that registers restarting the target's STS
					 * 
					 */
					try {
						decision = defauthzclient.authorize(deviceFoundCallback.getBulb().getDeviceReference(SecurityKey.EMPTY_KEY), deviceFoundCallback.getAuth().getDeviceReference(SecurityKey.EMPTY_KEY));
					} catch (AuthorizationException | CommunicationException
							| InvocationException e) {
						e.printStackTrace();
					}
				}

				Log.info("Authorization led to " + decision.toString());

				state = toggle_bulb;

				break;

			default:

				Log.error("OMG wrong state shiiiieeet: "+ state);
				state = check_existing_key;
			}

		}


	};








	/** prevents Authentication from being started if Button is still pushed after deleting key file **/
	public static void AuthenticationTriggerPreFilter(InPin switchPin){

		while(switchPin.getValue()==true){	// NOP until button is released

		}

		Timer.pause(200);                 // if Timer deleted -> exception couldn't write to IO-Pin

	}

	private static void pause(int ms){

		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

