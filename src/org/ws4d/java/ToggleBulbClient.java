package org.ws4d.java;

import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.incubation.Types.AuthenticationFault;
import org.ws4d.java.incubation.Types.AuthorizationFault;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import framboos.InPin;
import framboos.algorithm.util.Timer;

public class ToggleBulbClient {

	public static final int ERR_NOERROR = 0;
	public static final int ERR_BULB_NOT_FOUND = 1;
	public static final int ERR_AUTHORIZATION = 2;
	public static final int ERR_COMMUNICATION = 3;
	public static final int ERR_INVOCATION = 4;
	public static final int ERR_AUTHENTICATION = 5;

	private static int toggleBulb(Device bulbDev) {


		DefaultServiceReference lightBulbServiceReference = null;

		try {
			lightBulbServiceReference = (DefaultServiceReference) (bulbDev.getServiceReference(new URI("http://www.ws4d.org/LightBulbService"), SecurityKey.EMPTY_KEY));
		} catch (NullPointerException e){
			return ERR_INVOCATION;
		}

		Service lightBulbService = null;


		try {
			lightBulbService = lightBulbServiceReference.getService();
		} catch (CommunicationException e) {
			Log.error("Error! Could not find Light Bulb Service @ Device " + bulbDev.getEndpointReference().toString());
			return ERR_BULB_NOT_FOUND;
		}catch(NullPointerException e){
			Log.error("Error! (ServiceReference) NullPointer Ecxeption for " + bulbDev);
			return ERR_BULB_NOT_FOUND;
		};


		Operation stateOperation = lightBulbService.getOperation(null, "LightBulbState", null, null);
		Operation switchOperation = lightBulbService.getOperation(null, "LightBulbSwitch", null, null);

		ParameterValue stateIn = stateOperation.createInputValue();
		ParameterValue stateOut = null;
		try {
			stateOut = stateOperation.invoke(stateIn, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		} catch (AuthorizationException e) {
			Log.error("stateOperation: " + e.getMessage());
			return ERR_AUTHORIZATION;
		} catch (InvocationException e) {
			Log.error("stateOperation: " + e.getMessage());
			if (e.getCode().equals(AuthenticationFault.CLIENTNOTAUTHENTICATED)) {
				return ERR_AUTHENTICATION;
			} else if (e.getCode().equals(AuthorizationFault.CLIENTNOTAUTORIZED)) {
				return ERR_AUTHORIZATION;
			} else {
				return ERR_INVOCATION;
			}
		} catch (CommunicationException e) {
			e.printStackTrace();
			Log.error("stateOperation: " + e.getMessage());
			return ERR_COMMUNICATION;
		}
		String state = ParameterValueManagement.getString(stateOut, "state");
		String newState;

		if (state.toLowerCase().equals("off"))
			newState = "on";
		else
			newState = "off";

		ParameterValue switchIn = switchOperation.createInputValue();
		ParameterValue switchOut = null;
		ParameterValueManagement.setString(switchIn, "state", newState);

		try {
			switchOut = switchOperation.invoke(switchIn, CredentialInfo.EMPTY_CREDENTIAL_INFO, true, true);
		} catch (AuthorizationException e) {
			e.printStackTrace();
			Log.error("stateOperation: " + e.getMessage());
			return ERR_AUTHORIZATION;
		} catch (InvocationException e) {
			Log.error("stateOperation: " + e.getMessage());
			if (AuthenticationFault.CLIENTNOTAUTHENTICATED.equals(e.getCode()))
				return ERR_AUTHENTICATION;
			else if (AuthorizationFault.CLIENTNOTAUTORIZED.equals(e.getCode()))
				return ERR_AUTHORIZATION;
			else
				return ERR_INVOCATION;
		} catch (CommunicationException e) {
			e.printStackTrace();
			Log.error("stateOperation: " + e.getMessage());
			return ERR_COMMUNICATION;
		}

		Log.info("Successfully switched " + ParameterValueManagement.getString(switchOut, "state"));

		return ERR_NOERROR;
	}





	public static int startToggleBulbListener(Device bulb, LEDThread ledThread, InPin switchPin){

		int success = 0;
		int abort_count = 0;

		while(true){

			if(switchPin.getValue()==true){

				abort_count = 0;
				Timer.pause(40); // Entprellung

				while((switchPin.getValue()==true)&&(abort_count<700)){	// test if button is pushed for 7seconds
					abort_count++;
					pause(10);
				}


				if(abort_count>=700){					//if pushed for 10seconds: abort, else: try to switch bulb
					return 10;
				}

				if(bulb==null){								// test if DeviceInformations could be received above, if not: try again
					return ERR_BULB_NOT_FOUND;
				}else{													// DeviceInformation received: switch bulb!
					Log.info("[..H..] Trying to toggle bulb...");
					success = toggleBulb(bulb);				//actually toggling the  bulb
					if(success!=0){
						Log.error("[..H..] Switching Error - NOT successfully switched");
						return success;
					}
				}
			}



		}



	};




	public static Device getDeviceFromUUID(String UUID){
		Device theDevice = null;
		EndpointReference theDeviceEpr = null;
		DeviceReference theDeviceRef = null;

		theDeviceEpr = new EndpointReference(new AttributedURI(UUID));
		theDeviceRef = DeviceServiceRegistry.getDeviceReference(theDeviceEpr, SecurityKey.EMPTY_KEY, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		try {
			theDevice = theDeviceRef.getDevice();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return theDevice;
	}


	private static void pause(int ms){

		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



}
