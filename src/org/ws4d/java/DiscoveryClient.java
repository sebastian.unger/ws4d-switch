package org.ws4d.java;

import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.communication.structures.DiscoveryBinding;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wspolicy.securitypolicy.AuthenticationMechanismPolicy;
import org.ws4d.java.incubation.wspolicy.wspolicyclient.WSSecurityPolicyClient;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
//import framboos.algorithm.util.Timer;
import org.ws4d.java.util.Log;


public class DiscoveryClient extends DefaultClient {

	DeviceFoundInterface cb = null;


	//*********************  Constructor  **********************

	DiscoveryClient(DeviceFoundInterface deviceFoundCallback)
	{
		super();
		cb = deviceFoundCallback;
	}


	//************************ Methods  ***********************

	public static EndpointReference getEPR(Device device){

		if (!JMEDSFramework.isRunning()) {
			JMEDSFramework.start(null);
		}
		EndpointReference EPR = device.getEndpointReference();
		return EPR;
	}


	/**********************************************************
	 * 						Probe
	 **********************************************************/

	public void find_device_by_probe(String deviceName, String namespace) {
		find_device_by_probe(new QName(deviceName, namespace));
		return;
	}

	public void find_device_by_probe(QName deviceName) {

		if (!JMEDSFramework.isRunning()) {
			JMEDSFramework.start(null);
		}
		DPWSProperties.getInstance().removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);

		// create client
		// -> created in main

		//Use discovery, define what you are searching for and start search
		SearchParameter search = new SearchParameter();
		search.setDeviceTypes(new QNameSet(deviceName), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		Log.debug("[..H..] Starting Probe of " + deviceName);
		SearchManager.searchDevice(search, this, null);
		return;

	}


	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search, String comManId) {
		try {
			Device device = devRef.getDevice();
			Log.debug("[..H..] Found Device (Probe): " + device.getFriendlyName(LocalizedString.LANGUAGE_EN) + device.getEndpointReference());

			if(search.matchesDeviceTypes(new QNameSet(Main.LIGHTBULB_DEVICE_Q_NAME), comManId)){
				cb.deviceFound(Main.LIGHTBULB_DEVICE_Q_NAME, device);
			}

			if(search.matchesDeviceTypes(new QNameSet(Main.UIAUTHENTICATOR_DEVICE_Q_NAME), comManId)){
				/* check if Authenticator supports correct algorithm */

				Iterator it = device.getServiceReferences(new QNameSet(QName.construct(WSSecurityForDevicesConstants.STSAuthenticationECCDHBindingPortType)), SecurityKey.EMPTY_KEY);

				if (it == null || !it.hasNext()) {
					Log.warn("Found UI-Authenticator but it has no service with the correct binding. Skipping.");
					return;
				}

				ServiceReference servRef = (ServiceReference) it.next();
				Service s = servRef.getService();

				QNameSet targetMechanisms = AuthenticationMechanismPolicy.getAuthenticationMechanisms(s.getPolicy(new QName(WSSecurityForDevicesConstants.AuthenticationMechanismPolicyName)));
				QNameSet ownMechanisms = AuthenticationEngine.getInstance().getSupportedOOBauthenticationMechanisms();

				QName mech = WSSecurityPolicyClient.pickAuthenticationMechanism(ownMechanisms, targetMechanisms);

				if (mech == null) {
					Log.info("Found Device " + devRef.getEndpointReference().getAddress().toString()
							+ " but mechanisms do not match. Own: " + ownMechanisms
							+ ", target: " + targetMechanisms);
				} else {
					Log.info ("Picked " + mech);
					cb.deviceFound(Main.UIAUTHENTICATOR_DEVICE_Q_NAME, device);
				}
			}

			if(search.matchesDeviceTypes(new QNameSet(Main.AUTHENTICATOR_DEVICE_Q_NAME), comManId)){
				cb.deviceFound(Main.AUTHENTICATOR_DEVICE_Q_NAME, device);
			}


		} catch (CommunicationException e) {
			e.printStackTrace();
		}
	}







	/**********************************************************
	 * 						Hello
	 **********************************************************/

	public void start_hello_listener(String deviceName, String namespace) {
		start_hello_listener(new QName(deviceName, namespace));
		return;
	}



	public void start_hello_listener(QName deviceName){


		if (!JMEDSFramework.isRunning()) {
			JMEDSFramework.start(null);
		}


		DPWSProperties.getInstance().removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);
		// create client

		//Use discovery, define what you are searching for and start search
		SearchParameter search = new SearchParameter();
		search.setDeviceTypes(new QNameSet(deviceName), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		Log.info("[..H..] Starting HelloListening of " + deviceName);
		this.registerHelloListening(search);

		return;

	};


	@Override
	public void helloReceived(HelloData helloData) {

		try {

			Device device = getDeviceReference(helloData).getDevice();
			Log.debug("[..H..] Hello received from: " + device.toString());

			Iterator portTypeIterator = device.getPortTypes();
			Object currentPortType = new Object();

			while( portTypeIterator.hasNext()){						// lookup portTypes for bulb & authenticator

				currentPortType =  portTypeIterator.next();

				if(currentPortType.toString().equals(Main.LIGHTBULB_DEVICE_Q_NAME.toString())){
					cb.deviceFound((QName)currentPortType, device);
				}

				if(currentPortType.toString().equals(Main.UIAUTHENTICATOR_DEVICE_Q_NAME.toString())){
					cb.deviceFound((QName)currentPortType, device);
				}

				if(currentPortType.toString().equals(Main.AUTHENTICATOR_DEVICE_Q_NAME.toString())){
					cb.deviceFound((QName)currentPortType, device);
				}
			}

		} catch (CommunicationException e) {
			e.printStackTrace();
		}
	}

	private class DeviceCollector implements Runnable {
		@Override
		public void run() {
			while (!Thread.interrupted()) {

				find_device_by_probe(Main.LIGHTBULB_DEVICE_Q_NAME);
				find_device_by_probe(new QName(WSSecurityForDevicesConstants.UIAuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
				find_device_by_probe(new QName(WSSecurityForDevicesConstants.AuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));

				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					Log.info("Stopping Discovery thread.");
					break;
				}
			}

		}
	}

	Thread collector = null;

	public void start() {
		if (collector != null) {
			Log.info("Ignoring request to start Discovery-Thread");
			return;
		}
		collector = new Thread(new DeviceCollector());
		this.start_hello_listener(Main.LIGHTBULB_DEVICE_Q_NAME);
		this.start_hello_listener(new QName(WSSecurityForDevicesConstants.UIAuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
		this.start_hello_listener(new QName(WSSecurityForDevicesConstants.AuthenticatorType, WSSecurityForDevicesConstants.NAMESPACE));
		collector.start();
	}

	public void stop() {
		if (collector == null){
			Log.info("Ignoring request to stop Discovery-Thread");
			return;
		}
		/* WORKAROUND: simple unregisterHelloListening doesn't work due to bug in JMEDS */
		Iterator it = this.getDiscoveryBindingsForHelloListening();
		while (it.hasNext()) {
			DiscoveryBinding db = (DiscoveryBinding) it.next();
			this.unregisterHelloListening(db);
		}
		collector.interrupt();
		try {
			collector.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		collector = null;
	}




};
