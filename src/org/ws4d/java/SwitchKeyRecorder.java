package org.ws4d.java;

import org.ws4d.java.util.Log;

import framboos.InPin;
import framboos.algorithm.util.Timer;

public class SwitchKeyRecorder {
	

	private int counter = 0;						// Zaehler fuer Abtastroutine
	private String bin_key = new String();			// Aufgezeichneter Binaer Key
	private int int_key = 0;						// Binaerkey => Integer Key

	
	
	//*********************  Konstruktor  **********************
	
	private SwitchKeyRecorder()
	{
		super();
	}
	
	
	
	//************************ Methoden  ***********************
	
	public static int record_int_key(InPin switchPin, int key_length, int symbol_time)
	{
		SwitchKeyRecorder switchKeyRecorder = new SwitchKeyRecorder();		
		
		switchKeyRecorder.record_int_key_member(switchPin, key_length, symbol_time);
				
		return switchKeyRecorder.get_last_int_key();		
	}
	
	
	private int record_int_key_member(InPin switchPin, int key_length, int symbol_time)
	{
		bin_key = "";
		int_key= 0;		
		
		Log.info("[..H..] Waiting 4 Trigger");
		
		while(switchPin.getValue()==false){}       			// w8ing 4 trigger
		
		Log.info("[..H..] Recording...");   			
		
		Timer.pause((symbol_time/2)); 						// watch the center of current bit
		
		for(counter=0;counter<key_length;counter++)		 	// actually recording the key
		{
			Timer.pause(symbol_time);						// pause before - skipping start bit
			if(switchPin.getValue()==false)
			{
				Log.debug("[..H..]  0");   
				bin_key+='0';
			}else{
				Log.debug("[..H..]  1");   
				bin_key+='1';
			}	
			
		}
		
		
		Log.info("[..H..] bin_Key is: "+ bin_key); 		
		int_key =  Integer.parseInt(bin_key, 2); 			// 2 for binary
		Log.info("[..H..] int_Key is: "+ int_key); 		
				
		return int_key;		
	}
	
	
	
	private int get_last_int_key()
	{
		return this.int_key;		
	}
	
}


