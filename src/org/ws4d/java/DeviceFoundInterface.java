package org.ws4d.java;

import org.ws4d.java.service.Device;
import org.ws4d.java.types.QName;

public interface DeviceFoundInterface {
	public Device getBulb();
	public void resetBulb();
	public void overrideBulb(Device device);
	public Device getUIAuth();
	public void resetUIAuth();
	public void overrideUIAuth(Device device);
	public Device getAuth();
	public void resetAuth();
	public void overrideAuth(Device device);
	public boolean discoveryComplete();
	public boolean discoveryIncomplete();
	void deviceFound(QName type, Device device);
	public Device getAnyAuth();
}
